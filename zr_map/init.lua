-- map/init.lua

-- Mod global namespace

zr_map = {}


-- Load support for MT game translation.
local S = minetest.get_translator("zr_map")


-- Update HUD flags
-- Global to allow overriding

function zr_map.update_hud_flags(player)
	local creative_enabled = minetest.is_creative_enabled(player:get_player_name())

	local minimap_enabled = creative_enabled or
		player:get_inventory():contains_item("main", "zr_map:mapping_kit")
	local radar_enabled = creative_enabled

	player:hud_set_flags({
		minimap = minimap_enabled,
		minimap_radar = radar_enabled
	})
end


-- Set HUD flags 'on joinplayer'

minetest.register_on_joinplayer(function(player)
	zr_map.update_hud_flags(player)
end)


-- Cyclic update of HUD flags

local function cyclic_update()
	for _, player in ipairs(minetest.get_connected_players()) do
		zr_map.update_hud_flags(player)
	end
	minetest.after(5.3, cyclic_update)
end

minetest.after(5.3, cyclic_update)


-- Mapping kit item

minetest.register_craftitem("zr_map:mapping_kit", {
	description = S("Mapping Kit") .. "\n" .. S("Use with 'Minimap' key"),
	inventory_image = "zr_map_mapping_kit.png",
	stack_max = 1,
	groups = {flammable = 3},

	on_use = function(itemstack, user, pointed_thing)
		zr_map.update_hud_flags(user)
	end,
})
minetest.register_alias("map:mapping_kit", "zr_map:mapping_kit")
minetest.register_alias("mapping_kit", "zr_map:mapping_kit")


-- Crafting

minetest.register_craft({
	output = "zr_map:mapping_kit",
	recipe = {
		{"zr_glass:glass", "zr_paper:paper", "group:stick"},
		{"zr_iron:ingot", "zr_paper:paper", "zr_iron:ingot"},
		{"group:wood", "zr_paper:paper", "zr_dye:black"},
	}
})


-- Fuel

minetest.register_craft({
	type = "fuel",
	recipe = "zr_map:mapping_kit",
	burntime = 5,
})
