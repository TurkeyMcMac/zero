
local S = minetest.get_translator("zr_coal");

-- coal
minetest.register_node("zr_coal:ore", {
	description = S("Coal Ore"),
	tiles = {"zr_stone.png^zr_coal_mineral.png"},
	groups = {cracky = 3},
	drop = "zr_coal:lump",
	sounds = zr_stone.sounds,
})
minetest.register_alias("coal:ore", "zr_coal:ore")

minetest.register_node("zr_coal:block", {
	description = S("Coal Block"),
	tiles = {"zr_coal_block.png"},
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = zr_stone.sounds,
})
minetest.register_alias("coal:block", "zr_coal:block")

minetest.register_craftitem("zr_coal:lump", {
	description = S("Coal Lump"),
	inventory_image = "zr_coal_lump.png",
	groups = {coal = 1, flammable = 1}
})
minetest.register_alias("coal:lump", "zr_coal:lump")

minetest.register_craft({
	output = "zr_coal:lump 9",
	recipe = {
		{"zr_coal:block"},
	}
})
minetest.register_craft({
	type = "fuel",
	recipe = "zr_coal:lump",
	burntime = 40,
})

-- dye (if available)
if(minetest.get_modpath("zr_dye")) ~= nil then

	minetest.register_craft({
		output = "zr_dye:black 4",
		recipe = {
			{"group:coal"}
		},
	})
end

-- ore
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_coal:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 8 * 8 * 8,
	clust_num_ores = 9,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_coal:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 8 * 8 * 8,
	clust_num_ores = 8,
	clust_size     = 3,
	y_max          = 64,
	y_min          = -127,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_coal:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 12 * 12 * 12,
	clust_num_ores = 30,
	clust_size     = 5,
	y_max          = -128,
	y_min          = -31000,
})
