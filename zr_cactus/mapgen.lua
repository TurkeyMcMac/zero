
local schematic_path = minetest.get_modpath("zr_cactus").."/schematics/"

function zr_cactus.add_to_biome(biome, def)
	local cactus_def = {
		name = biome..":cactus",
		deco_type = "simple",
		place_on = {"zr_sand:desert"},
		sidelen = 16,
		noise_params = {
			offset = -0.0003,
			scale = 0.0009,
			spread = {x = 200, y = 200, z = 200},
			seed = 230,
			octaves = 3,
			persist = 0.6
		},
		biomes = { biome },
		y_max = 31000,
		y_min = 4,
		decoration = "zr_cactus:cactus",
		height = 2,
		height_max = 5,
	}
	def = def or {}
	for k, v in pairs(def) do cactus_def[k] = v end

	minetest.register_decoration(cactus_def)
end

function zr_cactus.add_large_to_biome(biome, def)
	local cactus_def = {
		name = biome..":large_cactus",
		deco_type = "schematic",
		place_on = {"zr_sand:desert"},
		sidelen = 16,
		noise_params = {
			offset = -0.0003,
			scale = 0.0009,
			spread = {x = 200, y = 200, z = 200},
			seed = 230,
			octaves = 3,
			persist = 0.6
		},
		biomes = { biome },
		y_max = 31000,
		y_min = 4,
		schematic = schematic_path.."zr_cactus_large.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
	}

	def = def or {}
	for k, v in pairs(def) do cactus_def[k] = v end

	minetest.register_decoration(cactus_def)
end

function zr_cactus.add_all_to_biome(biome, def)
	zr_cactus.add_to_biome(biome,def)
	zr_cactus.add_large_to_biome(biome,def)
end
