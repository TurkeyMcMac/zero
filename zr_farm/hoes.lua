-- support for MT game translation.
local S = minetest.get_translator("zr_farm")

zr_farm.register_hoe(":zr_farm:hoe_wood", {
	description = S("Wooden Hoe"),
	inventory_image = "zr_farm_tool_woodhoe.png",
	max_uses = 30,
	material = "group:wood",
	groups = {hoe = 1, flammable = 2},
})

zr_farm.register_hoe(":zr_farm:hoe_stone", {
	description = S("Stone Hoe"),
	inventory_image = "zr_farm_tool_stonehoe.png",
	max_uses = 90,
	material = "group:stone",
	groups = {hoe = 1}
})

zr_farm.register_hoe(":zr_farm:hoe_steel", {
	description = S("Steel Hoe"),
	inventory_image = "zr_farm_tool_steelhoe.png",
	max_uses = 500,
	material = "zr_iron:ingot",
	groups = {hoe = 1}
})

