local S = minetest.get_translator("zr_diamond");

minetest.register_node("zr_diamond:ore", {
	description = S("Diamond Ore"),
	tiles = {"zr_stone.png^zr_diamond_mineral.png"},
	groups = {cracky = 1},
	drop = "zr_diamond:diamond",
	sounds = zr_stone.sounds,
})
minetest.register_alias("diamond:ore", "zr_diamond:ore")

minetest.register_node("zr_diamond:block", {
	description = S("Diamond Block"),
	tiles = {"zr_diamond_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level = 3},
	sounds = zr_stone.sounds,
})
minetest.register_alias("diamond:block", "zr_diamond:block")

-- crafting
minetest.register_craftitem("zr_diamond:diamond", {
	description = S("Diamond"),
	inventory_image = "zr_diamond.png",
})
minetest.register_alias("diamond:diamond", "zr_diamond:diamond")
minetest.register_alias("diamond", "zr_diamond:diamond")

minetest.register_craft({
	output = "zr_diamond:diamond 9",
	recipe = {
		{"zr_diamond:block"},
	}
})

minetest.register_craft({
	output = "zr_diamond:block",
	recipe = {
		{"zr_diamond:diamond", "zr_diamond:diamond", "zr_diamond:diamond"},
		{"zr_diamond:diamond", "zr_diamond:diamond", "zr_diamond:diamond"},
		{"zr_diamond:diamond", "zr_diamond:diamond", "zr_diamond:diamond"},
	}
})

-- ore distribution
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_diamond:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 15 * 15 * 15,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_diamond:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 17 * 17 * 17,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -1024,
	y_min          = -2047,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_diamond:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 15 * 15 * 15,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -2048,
	y_min          = -31000,
})

-- tools

if(minetest.get_modpath("zr_tools")) ~= nil then

	zr_tools.register_pickaxe("zr_diamond:pick", {
		description = S("Diamond Pickaxe"),
		inventory_image = "zr_diamond_pick.png",
		tool_capabilities = {
			full_punch_interval = 0.9,
			max_drop_level=3,
			groupcaps={
				cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=30, maxlevel=3},
			},
			damage_groups = {fleshy=5},
		},
		recipeitem = "zr_diamond:diamond",
	})
	minetest.register_alias("diamond:pick", "zr_diamond:pick")

	zr_tools.register_shovel("zr_diamond:shovel", {
		description = S("Diamond Shovel"),
		inventory_image = "zr_diamond_shovel.png",
		wield_image = "zr_diamond_shovel.png^[transformR90",
		tool_capabilities = {
			full_punch_interval = 1.0,
			max_drop_level=1,
			groupcaps={
				crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=30, maxlevel=3},
			},
			damage_groups = {fleshy=4},
		},
		recipeitem = "zr_diamond:diamond",
	})
	minetest.register_alias("diamond:shovel", "zr_diamond:shovel")

	zr_tools.register_axe("zr_diamond:axe", {
		description = S("Diamond Axe"),
		inventory_image = "zr_diamond_axe.png",
		tool_capabilities = {
			full_punch_interval = 0.9,
			max_drop_level=1,
			groupcaps={
				choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=30, maxlevel=3},
			},
			damage_groups = {fleshy=7},
		},
		recipeitem = "zr_diamond:diamond",
	})
	minetest.register_alias("diamond:axe", "zr_diamond:axe")

	zr_tools.register_sword("zr_diamond:sword", {
		description = S("Diamond Sword"),
		inventory_image = "zr_diamond_sword.png",
		tool_capabilities = {
			full_punch_interval = 0.7,
			max_drop_level=1,
			groupcaps={
				snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, uses=40, maxlevel=3},
			},
			damage_groups = {fleshy=8},
		},
		recipeitem = "zr_diamond:diamond",
	})
	minetest.register_alias("diamond:sword", "zr_diamond:sword")
end
