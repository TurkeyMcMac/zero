local S = minetest.get_translator("zr_wood");

zr_wood.sounds = {
	footstep = {name = "zr_wood_footstep", gain = 0.3},
	dig = {name = "zr_wood_dig", gain = 0.5},
	dug = {name = "zr_wood_dug", gain = 1.0},
	place = {name = "zr_wood_place", gain = 0.5},
}

minetest.register_craftitem("zr_wood:stick", {
    description = S("Stick"),
    inventory_image = "zr_wood_stick.png",
    groups = {stick = 1, flammable = 2}, 
})
minetest.register_alias("wood:stick", "zr_wood:stick")
minetest.register_alias("stick", "zr_wood:stick")

minetest.register_craft({
    output = "zr_wood:stick 4",
    recipe = {
        {"group:wood"},
    }
})

minetest.register_craft({
    type = "fuel",
    recipe = "group:wood",
    burntime = 7,
})

minetest.register_node("zr_wood:ladder", {
	description = S("Wooden Ladder"),
	drawtype = "signlike",
	tiles = {"zr_wood_ladder.png"},
	inventory_image = "zr_wood_ladder.png",
	wield_image = "zr_wood_ladder.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	climbable = true,
	is_ground_content = false,
	selection_box = {
		type = "wallmounted",
		--wall_top = = <default>
		--wall_bottom = = <default>
		--wall_side = = <default>
	},
	groups = {choppy = 2, oddly_breakable_by_hand = 3, flammable = 2},
	legacy_wallmounted = true,
	sounds = zr_wood.sounds,
})
minetest.register_alias("wood:ladder", "zr_wood:ladder")

minetest.register_craft({
	output = "zr_wood:ladder 5",
	recipe = {
		{"group:stick", "", "group:stick"},
		{"group:stick", "group:stick", "group:stick"},
		{"group:stick", "", "group:stick"},
	}
})

minetest.register_craft({
	type = "fuel",
	recipe = "zr_wood:ladder",
	burntime = 7,
})

