
local S = minetest.get_translator("zr_aspen");

-- FENCE
zr_fence.register_fence("zr_aspen:fence", {                                                                       
    description = S("Aspen Wood Fence"),
    texture = "zr_aspen_fence.png",
    material = "zr_aspen:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("aspen:fence", "zr_aspen:fence")

zr_fence.register_fence_rail("zr_aspen:fence_rail", {                                                                       
    description = S("Aspen Wood Fence Rail"),
    texture = "zr_aspen_fence_rail.png",
    material = "zr_aspen:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("aspen:fence_rail", "zr_aspen:fence_rail")
