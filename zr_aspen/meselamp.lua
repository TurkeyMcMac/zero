
local S = minetest.get_translator("zr_aspen");

zr_mese.register_lampost("zr_aspen:mese_post_light", {
	description = S("Aspen Wood Mese Post Light"),
	texture = "zr_aspen_fence.png",
	material = "zr_aspen:wood",
})
minetest.register_alias("aspen:mese_post_light", "zr_aspen:mese_post_light")
