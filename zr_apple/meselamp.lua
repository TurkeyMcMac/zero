
local S = minetest.get_translator("zr_apple");

zr_mese.register_lampost("zr_apple:mese_post_light", {
	description = S("Apple Wood Mese Post Light"),
	texture = "zr_apple_fence.png",
	material= "zr_apple:wood",
})
minetest.register_alias("apple:mese_post_light", "zr_apple:mese_post_light")
