local S = minetest.get_translator("zr_apple");

function zr_apple.grow_sapling(pos) 

	if not zr_wood.check_grow_tree(pos) then
        minetest.get_node_timer(pos):start(math.random(300, 1500))
		return
	end

    local tree_schema = minetest.get_modpath("zr_apple") .. "/schematics/zr_apple_tree_from_sapling.mts"
    minetest.place_schematic({x=pos.x-3, y=pos.y-1, z=pos.z-3}, tree_schema, "random", nil, false)
end

minetest.register_node("zr_apple:sapling", {
    description = S("Apple Tree Sapling"),
    drawtype = "plantlike",
    tiles = {"zr_apple_sapling.png"},
    inventory_image = "zr_apple_sapling.png",
    wield_image = "zr_apple_sapling.png",
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    on_timer = zr_apple.grow_sapling,
    selection_box = {
        type = "fixed",
        fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
    },
    groups = {snappy = 2, dig_immediate = 3, flammable = 2,
        attached_node = 1, sapling = 1},
    sounds = zr_wood.leaves_sounds,

    on_construct = function(pos)
        minetest.get_node_timer(pos):start(math.random(300, 1500))
    end,

    on_place = function(itemstack, placer, pointed_thing)
        itemstack = zr_wood.sapling_on_place(itemstack, placer, pointed_thing,
            "zr_apple:sapling",
            -- minp_relative.y = 1 because sapling pos has been checked
            {x = -3, y = 1, z = -3},
            {x = 3, y = 6, z = 3},
            -- maximum interval of interior volume check
            4)

        return itemstack
    end,
})
minetest.register_alias("apple:sapling", "zr_apple:sapling")

minetest.register_lbm({
    name = "zr_apple:convert_saplings_to_apple_tree",
    nodenames = {"zr_apple:sapling"},
    action = function(pos)
        minetest.get_node_timer(pos):start(math.random(300, 1500))
    end
})

