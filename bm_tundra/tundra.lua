minetest.register_biome({
	name = "bm_tundra_highland",
	node_dust = "zr_snow:snow",
	node_riverbed = "zr_gravel:gravel",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = 31000,
	y_min = 47,
	heat_point = 0,
	humidity_point = 40,
})

minetest.register_biome({
	name = "bm_tundra",
	node_top = "zr_snow:permafrost_with_stones",
	depth_top = 1,
	node_filler = "zr_snow:permafrost",
	depth_filler = 1,
	node_riverbed = "zr_gravel:gravel",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	vertical_blend = 4,
	y_max = 46,
	y_min = 2,
	heat_point = 0,
	humidity_point = 40,
})

minetest.register_biome({
	name = "bm_tundra_beach",
	node_top = "zr_gravel:gravel",
	depth_top = 1,
	node_filler = "zr_gravel:gravel",
	depth_filler = 2,
	node_riverbed = "zr_gravel:gravel",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	vertical_blend = 1,
	y_max = 1,
	y_min = -3,
	heat_point = 0,
	humidity_point = 40,
})

minetest.register_biome({
	name = "bm_tundra_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_riverbed = "zr_gravel:gravel",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	vertical_blend = 1,
	y_max = -4,
	y_min = -255,
	heat_point = 0,
	humidity_point = 40,
})

local cave_liquid = {"zr_water:source"};
if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_tundra_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = -256,
	y_min = -31000,
	heat_point = 0,
	humidity_point = 40,
})

zr_snow.add_permafrost_to_biome("bm_tundra")
zr_snow.add_to_biome("bm_tundra", {
	biomes = {"bm_tundra", "bm_tundra_beach"},
})
