
local S = minetest.get_translator("zr_acacia");

-- FENCE
zr_fence.register_fence("zr_acacia:fence", {                                                                       
    description = S("Acacia Wood Fence"),
    texture = "zr_acacia_fence.png",
    material = "zr_acacia:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})

zr_fence.register_fence_rail("zr_acacia:fence_rail", {                                                                       
    description = S("Acacia Wood Fence Rail"),
    texture = "zr_acacia_fence_rail.png",
    material = "zr_acacia:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
