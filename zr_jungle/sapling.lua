local S = minetest.get_translator("zr_jungle");

local function start_timer(pos)
	minetest.get_node_timer(pos):start(math.random(300, 1500))
end

function zr_jungle.grow_jungle_sapling(pos) 

	if not zr_wood.check_grow_tree(pos) then
		start_timer(pos)
		return
	end

    local tree_schema = minetest.get_modpath("zr_jungle") .. "/schematics/zr_jungle_tree_from_sapling.mts"
    minetest.place_schematic({x=pos.x-4, y=pos.y-1, z=pos.z-4}, tree_schema, "random", nil, false)
end

minetest.register_node("zr_jungle:sapling", {
    description = S("Jungle Tree Sapling"),
    drawtype = "plantlike",
    tiles = {"zr_jungle_sapling.png"},
    inventory_image = "zr_jungle_sapling.png",
    wield_image = "zr_jungle_sapling.png",
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    on_timer = zr_jungle.grow_jungle_sapling,
    selection_box = {
        type = "fixed",
        fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
    },
    groups = {snappy = 2, dig_immediate = 3, flammable = 2,
        attached_node = 1, sapling = 1},
    sounds = zr_wood.leaves_sounds,

    on_construct = start_timer,

    on_place = function(itemstack, placer, pointed_thing)
        itemstack = zr_wood.sapling_on_place(itemstack, placer, pointed_thing,
            "zr_jungle:sapling",
            -- minp_relative.y = 1 because sapling pos has been checked
            {x = -3, y = 1, z = -3},
            {x = 3, y = 6, z = 3},
            -- maximum interval of interior volume check
            4)

        return itemstack
    end,
})
minetest.register_alias("jungle:sapling", "zr_jungle:sapling")

minetest.register_lbm({
    name = "zr_jungle:grow_sapling",
    nodenames = {"zr_jungle:sapling"},
    action = start_timer,
})

