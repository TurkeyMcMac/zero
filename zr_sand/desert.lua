local S = minetest.get_translator("zr_sand")

minetest.register_node("zr_sand:desert", {
	description = S("Desert Sand"),
	tiles = {"zr_sand_desert.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
    sounds = zr_sand.sounds,
})
minetest.register_alias("sand:desert", "zr_sand:desert")

minetest.register_node("zr_sand:desert_stone", {
	description = S("Desert Stone"),
	tiles = {"zr_sand_desert_stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = "zr_sand:desert_cobble",
	legacy_mineral = true,
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_stone", "zr_sand:desert_stone")

minetest.register_node("zr_sand:desert_cobble", {
	description = S("Desert Cobblestone"),
	tiles = {"zr_sand_desert_cobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 2},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_cobble", "zr_sand:desert_cobble")

minetest.register_node("zr_sand:desert_stone_brick", {
	description = S("Desert Stone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_sand_desert_stone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_stone_brick", "zr_sand:desert_stone_brick")

minetest.register_node("zr_sand:desert_stone_block", {
	description = S("Desert Stone Block"),
	tiles = {"zr_sand_desert_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_stone_block", "zr_sand:desert_stone_block")


minetest.register_node("zr_sand:desert_sandstone", {
	description = S("Desert Sandstone"),
	tiles = {"zr_sand_desert_sandstone.png"},
	groups = {crumbly = 1, cracky = 3},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_sandstone", "zr_sand:desert_sandstone")

minetest.register_node("zr_sand:desert_sandstone_brick", {
	description = S("Desert Sandstone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_sand_desert_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_sandstone_brick", "zr_sand:desert_sandstone_brick")

minetest.register_node("zr_sand:desert_sandstone_block", {
	description = S("Desert Sandstone Block"),
	tiles = {"zr_sand_desert_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:desert_sandstone_block", "zr_sand:desert_sandstone_block")
