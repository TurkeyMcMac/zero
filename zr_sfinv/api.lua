
zr_sfinv.pages = {}
zr_sfinv.pages_unordered = {}
zr_sfinv.contexts = {}
zr_sfinv.enabled = true

function zr_sfinv.register_page(name, def)
	assert(name, "Invalid sfinv page. Requires a name")
	assert(def, "Invalid sfinv page. Requires a def[inition] table")
	assert(def.get, "Invalid sfinv page. Def requires a get function.")
	assert(not zr_sfinv.pages[name], "Attempt to register already registered sfinv page " .. dump(name))

	zr_sfinv.pages[name] = def
	def.name = name
	table.insert(zr_sfinv.pages_unordered, def)
end

function zr_sfinv.override_page(name, def)
	assert(name, "Invalid sfinv page override. Requires a name")
	assert(def, "Invalid sfinv page override. Requires a def[inition] table")
	local page = zr_sfinv.pages[name]
	assert(page, "Attempt to override sfinv page " .. dump(name) .. " which does not exist.")
	for key, value in pairs(def) do
		page[key] = value
	end
end

function zr_sfinv.get_nav_fs(player, context, nav, current_idx)
	-- Only show tabs if there is more than one page
	if #nav > 1 then
		return "tabheader[0,0g;" .. table.concat(nav, ",") ..
				";" .. current_idx .. ";true;false]"
	else
		return ""
	end
end

local theme_inv = [[
		image[0,5.2;1,1;zr_gui_hb_bg.png]
		image[1,5.2;1,1;zr_gui_hb_bg.png]
		image[2,5.2;1,1;zr_gui_hb_bg.png]
		image[3,5.2;1,1;zr_gui_hb_bg.png]
		image[4,5.2;1,1;zr_gui_hb_bg.png]
		image[5,5.2;1,1;zr_gui_hb_bg.png]
		image[6,5.2;1,1;zr_gui_hb_bg.png]
		image[7,5.2;1,1;zr_gui_hb_bg.png]
		list[current_player;main;0,5.2;8,1;]
		list[current_player;main;0,6.35;8,3;8]
	]]

function zr_sfinv.make_formspec(player, context, content, show_inv, size)
	local tmp = {
		size or "size[8,9.1]",
		zr_sfinv.get_nav_fs(player, context, context.nav_titles, context.nav_idx),
		show_inv and theme_inv or "",
		content
	}
	return table.concat(tmp, "")
end

function zr_sfinv.get_homepage_name(player)
	return "sfinv:crafting"
end

function zr_sfinv.get_formspec(player, context)
	-- Generate navigation tabs
	local nav = {}
	local nav_ids = {}
	local current_idx = 1
	for i, pdef in pairs(zr_sfinv.pages_unordered) do
		if not pdef.is_in_nav or pdef:is_in_nav(player, context) then
			nav[#nav + 1] = pdef.title
			nav_ids[#nav_ids + 1] = pdef.name
			if pdef.name == context.page then
				current_idx = #nav_ids
			end
		end
	end
	context.nav = nav_ids
	context.nav_titles = nav
	context.nav_idx = current_idx

	-- Generate formspec
	local page = zr_sfinv.pages[context.page] or zr_sfinv.pages["404"]
	if page then
		return page:get(player, context)
	else
		local old_page = context.page
		local home_page = zr_sfinv.get_homepage_name(player)

		if old_page == home_page then
			minetest.log("error", "[zr_sfinv] Couldn't find " .. dump(old_page) ..
					", which is also the old page")

			return ""
		end

		context.page = home_page
		assert(zr_sfinv.pages[context.page], "[zr_sfinv] Invalid homepage")
		minetest.log("warning", "[zr_sfinv] Couldn't find " .. dump(old_page) ..
				" so switching to homepage")

		return zr_sfinv.get_formspec(player, context)
	end
end

function zr_sfinv.get_or_create_context(player)
	local name = player:get_player_name()
	local context = zr_sfinv.contexts[name]
	if not context then
		context = {
			page = zr_sfinv.get_homepage_name(player)
		}
		zr_sfinv.contexts[name] = context
	end
	return context
end

function zr_sfinv.set_context(player, context)
	zr_sfinv.contexts[player:get_player_name()] = context
end

function zr_sfinv.set_player_inventory_formspec(player, context)
	local fs = zr_sfinv.get_formspec(player,
			context or zr_sfinv.get_or_create_context(player))
	player:set_inventory_formspec(fs)
end

function zr_sfinv.set_page(player, pagename)
	local context = zr_sfinv.get_or_create_context(player)
	local oldpage = zr_sfinv.pages[context.page]
	if oldpage and oldpage.on_leave then
		oldpage:on_leave(player, context)
	end
	context.page = pagename
	local page = zr_sfinv.pages[pagename]
	if page.on_enter then
		page:on_enter(player, context)
	end
	zr_sfinv.set_player_inventory_formspec(player, context)
end

function zr_sfinv.get_page(player)
	local context = zr_sfinv.contexts[player:get_player_name()]
	return context and context.page or zr_sfinv.get_homepage_name(player)
end

minetest.register_on_joinplayer(function(player)
	if zr_sfinv.enabled then
		zr_sfinv.set_player_inventory_formspec(player)
	end
end)

minetest.register_on_leaveplayer(function(player)
	zr_sfinv.contexts[player:get_player_name()] = nil
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "" or not zr_sfinv.enabled then
		return false
	end

	-- Get Context
	local name = player:get_player_name()
	local context = zr_sfinv.contexts[name]
	if not context then
		zr_sfinv.set_player_inventory_formspec(player)
		return false
	end

	-- Was a tab selected?
	if fields.zr_sfinv_nav_tabss and context.nav then
		local tid = tonumber(fields.zr_sfinv_nav_tabss)
		if tid and tid > 0 then
			local id = context.nav[tid]
			local page = zr_sfinv.pages[id]
			if id and page then
				zr_sfinv.set_page(player, id)
			end
		end
	else
		-- Pass event to page
		local page = zr_sfinv.pages[context.page]
		if page and page.on_player_receive_fields then
			return page:on_player_receive_fields(player, context, fields)
		end
	end
end)
