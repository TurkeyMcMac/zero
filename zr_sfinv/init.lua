-- sfinv/init.lua

zr_sfinv = {}
sfinv = zr_sfinv -- alias for compatibility

dofile(minetest.get_modpath("zr_sfinv") .. "/api.lua")

-- Load support for MT game translation.
local S = minetest.get_translator("zr_sfinv")

zr_sfinv.register_page("sfinv:crafting", {
	title = S("Crafting"),
	get = function(self, player, context)
		return zr_sfinv.make_formspec(player, context, [[
				list[current_player;craft;1.75,0.5;3,3;]
				list[current_player;craftpreview;5.75,1.5;1,1;]
				image[4.75,1.5;1,1;zr_sfinv_crafting_arrow.png]
				listring[current_player;main]
				listring[current_player;craft]
			]], true)
	end
})
